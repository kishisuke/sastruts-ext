package com.acromo.sastruts;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

public class ParamAliasDescTest {

    @Test
    public void test() {
        ParamAliasDesc paramAliasDesc = new ParamAliasDesc(TestBean.class);
        assertThat(paramAliasDesc.getReal("val1_alias"), is("val1"));
        assertThat(paramAliasDesc.getReal("val2_alias"), is("val2"));
        assertThat(paramAliasDesc.getReal("val3"), nullValue());
    }

    @Test
    public void testGet() {
        ParamAliasDesc paramAliasDesc = ParamAliasDesc.get(TestBean.class);
        assertThat(paramAliasDesc.getReal("val1_alias"), is("val1"));
        assertThat(paramAliasDesc.getReal("val2_alias"), is("val2"));
        assertThat(paramAliasDesc.getReal("val3"), nullValue());

        assertThat(ParamAliasDesc.get(TestBean.class),
                is(ParamAliasDesc.get(TestBean.class)));
    }

    private static class TestBean {

        @ParamAlias("val1_alias")
        public String val1;
        private String val2;
        @SuppressWarnings("unused")
        public String val3;

        @ParamAlias("val2_alias")
        public String getVal2() {
            return val2;
        }

    }

}
