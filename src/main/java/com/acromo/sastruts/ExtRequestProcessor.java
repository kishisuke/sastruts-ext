package com.acromo.sastruts;

import java.util.Map;

import org.seasar.struts.action.S2RequestProcessor;

public class ExtRequestProcessor extends S2RequestProcessor {

    @Override
    protected Object getSimpleProperty(Object bean, String name) {
        if (bean instanceof Map) {
            return ((Map<?, ?>) bean).get(name);
        }
        return super.getSimpleProperty(bean, getName(bean, name));
    }

    @Override
    protected void setSimpleProperty(Object bean, String name, Object value) {
        super.setSimpleProperty(bean, getName(bean, name), value);
    }

    @Override
    protected Object getIndexedProperty(Object bean, String name, int[] indexes) {
        return super.getIndexedProperty(bean, getName(bean, name), indexes);
    }

    @Override
    protected void setIndexedProperty(Object bean, String name, int[] indexes,
            Object value) {
        super.setIndexedProperty(bean, getName(bean, name), indexes, value);
    }

    private String getName(Object bean, String name) {
        String _name = ParamAliasDesc.get(bean.getClass()).getReal(name);
        return (_name != null) ? _name : name;
    }

}
