package com.acromo.sastruts;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.seasar.framework.beans.BeanDesc;
import org.seasar.framework.beans.PropertyDesc;
import org.seasar.framework.beans.factory.BeanDescFactory;

public class ParamAliasDesc {

    private static ConcurrentMap<Class<?>, ParamAliasDesc> cache = new ConcurrentHashMap<Class<?>, ParamAliasDesc>(
            1024);

    private Map<String, String> map = new HashMap<String, String>();

    public static ParamAliasDesc get(Class<?> beanClass) {
        ParamAliasDesc paramAliasDesc = cache.get(beanClass);
        if (paramAliasDesc != null) {
            return paramAliasDesc;
        }
        return putIfAbsent(beanClass, new ParamAliasDesc(beanClass));
    }

    private static ParamAliasDesc putIfAbsent(Class<?> beanClass,
            ParamAliasDesc newObj) {
        ParamAliasDesc oldObj = cache.putIfAbsent(beanClass, newObj);
        if (oldObj != null) {
            return oldObj;
        }
        return newObj;
    }

    public ParamAliasDesc(Class<?> beanClass) {
        BeanDesc beanDesc = BeanDescFactory.getBeanDesc(beanClass);
        int propertyDescSize = beanDesc.getPropertyDescSize();
        for (int i = 0; i < propertyDescSize; i++) {
            PropertyDesc propertyDesc = beanDesc.getPropertyDesc(i);
            String alias = getAlias(propertyDesc);
            if (alias != null) {
                map.put(alias, propertyDesc.getPropertyName());
            }
        }
    }

    public String getReal(String alias) {
        return map.get(alias);
    }

    private String getAlias(PropertyDesc propertyDesc) {
        if (!propertyDesc.isReadable()) {
            return null;
        }
        Field field = propertyDesc.getField();
        if (field != null && field.isAnnotationPresent(ParamAlias.class)) {
            return field.getAnnotation(ParamAlias.class).value();
        }
        Method readMethod = propertyDesc.getReadMethod();
        if (readMethod != null
                && readMethod.isAnnotationPresent(ParamAlias.class)) {
            return readMethod.getAnnotation(ParamAlias.class).value();
        }
        return null;
    }

}
